class Type:
    def __init__(self, cls, name=None):
        self._isOptional = False

        if isinstance(cls, Type):
            name = cls._name
            self._isOptional = cls._isOptional
            self._secondaryType = cls._secondaryType
            cls = cls._cls

        if not isinstance(cls, type):
            cls = type(cls)

        self._cls = cls

        if name is None:
            self._name = cls.__name__
        else:
            self._name = name

        self._secondaryType = None

    def __repr__(self):
        s = self._name

        if self._secondaryType is not None:
            s = s + ' of ' + str(self._secondaryType)

        if self._isOptional:
            s = 'optional({})'.format(s)

        return s

    @staticmethod
    def isIterable(item):
        try:
            iter(item)
        except TypeError:
            return False
        return True

    @staticmethod
    def assertImplements(a, B):
        if not hasattr(a, '_py3typing_implements'):
            raise NotImplementedError(str(a.__class__) + ' does not implement any class.'
                + ' Check that you are using @Implements.')

        for method in dir(B):
            g = getattr(B, method)
            if callable(g):
                if not hasattr(a, method):
                    raise NotImplementedError(str(a.__class__) + ' does not implement ' + method)
                f = getattr(a, method)
                Type.assertReturnSignatureMatches(f, g)
                Type.assertParameterSignatureMatches(f, g)

    @staticmethod
    def assertParameterSignatureMatches(f, g):
        if hasattr(g, '__params__'):
            if not hasattr(f, '__params__'):
                raise TypeError(f.__name__ + ' does not have parameter types: ' + str(g.__params__))
            if not f.__params__ == g.__params__:
                raise TypeError(f.__name__ + '\'s argument types: ' + str(f.__params__)
                + ' does not match ' + str(g.__params__))
    @staticmethod
    def assertReturnSignatureMatches(f, g):
        if hasattr(g, '__returntype__'):
            if not hasattr(f, '__returntype__'):
                raise TypeError(f.__name__ + ' does not have return type: ' + str(g.__returntype__))
            if not f.__returntype__ == g.__returntype__:
                raise TypeError(f.__name__ + '\'s return type: ' + str(f.__returntype__)
                + ' does not match ' + str(g.__returntype__))

    def of(self, secondaryType):
        result = type(self)(self._cls, self._name)

        if not isinstance(secondaryType, Type):
            secondaryType = Type(secondaryType)

        result._secondaryType = secondaryType

        return result

    def matches(self, instance):
        if not self._secondaryType is None:
            if not Type.isIterable(instance):
                raise TypeError(str(instance) + ' is not iterable. Expected: ' + str(self))
            for item in instance:
                if not self._secondaryType.matches(item):
                    return False

        try:
            Type.assertImplements(instance, self._cls)
        except RuntimeError:
            if self._isOptional and instance is None:
                return True

            return isinstance(instance, self._cls) 

        return True

    def named(self, name):
        self._name = name
        return self

    def __eq__(self, other):
        if other is None:
            return False
        return issubclass(self._cls, other._cls) and self._secondaryType == other._secondaryType
