from py3typing.Type import Type

class Iterable(Type):
    def __init__(self, cls=None, name=None):
        self._isOptional = False
        if not isinstance(cls, type):
            cls = type(cls)
        
        if name is not None:
            self._name = name
        else:
            self._name = cls.__name__

        self._cls = cls
        self._secondaryType = None

    def matches(self, instance):
        if not self._secondaryType is None:
            if not Type.isIterable(instance):
                raise TypeError(str(instance) + ' is not iterable. Expected: ' + str(self))

        if self._cls is not type(None):
            try:
                Type.assertImplements(instance, self._cls)
            except RuntimeError:
                if not isinstance(instance, self._cls): 
                    return False

        if self._secondaryType is not None:
            return TypeCheckingIterator(instance, self._secondaryType)

        return instance

class TypeCheckingIterator(Iterable):
    def __init__(self, data, t):
        super().__init__(t, 'TypeCheckingIterator')

        self._data = iter(data)
        self._type = t

    def __iter__(self):
        return self

    def __next__(self):
        result = next(self._data)

        if not self._type.matches(result):
            raise TypeError('Unexpected type during iteration. Expected '
                + str(self._type) + ', but got ' + str(result.__class__))

        return result