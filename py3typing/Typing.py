'''
    Copyright 2016 Google Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
'''

from py3typing.Type import Type
from py3typing.Function import Function
from py3typing.Iterable import *
from py3typing.Method import Method

class Py3TypingError(RuntimeError):
    pass

class TypingConfig:
    def __init__(self):
        self._enabled = False

    def isEnabled(self):
        return self._enabled

    def setEnabled(self, value):
        self._enabled = value

Typing = TypingConfig()

def optional(t):
    result = Type(t)
    result._isOptional = True

    return result

class EnclosingClassType(Type):
    pass

LIST = Type(list)
VECTOR = LIST.of(float).named('vector')

class Implements(object):
    def __init__(self, cls):
        self._cls = cls

    def __call__(self, C):
        if not Typing.isEnabled():
            return C

        setattr(C, '_py3typing_implements', True)

        constructor = getattr(C, '__new__')

        def f(cls, *args):
            instance = constructor(C)

            Type.assertImplements(instance, self._cls)

            return instance

        setattr(C, '__new__', f)

        return C

class Returns(object):
    @staticmethod
    def disable():
        Returns._enabled = False

    def __init__(self, returns):
        if not isinstance(returns, Type):
            returns = Type(returns)

        self._returns = returns

        assert(isinstance(self._returns, Type))

    def __call__(self, f):
        if not Typing.isEnabled():
            return f

        def typedF(*args, **kwargs):
            result = f(*args, **kwargs)

            typeCheckResult = self._returns.matches(result)
            
            if type(typeCheckResult) is TypeCheckingIterator:
                return typeCheckResult

            if not typeCheckResult:
                raise TypeError('Invalid return type for ' + f.__name__ + '. Expected: '
                + str(self._returns)
                + ', got: ' + str(result.__class__))

            return result

        typedF.__name__ = getattr(f, '__name__', 'typedF')
        if f.__doc__ is None:
            f.__doc__ = ''
        typedF.__doc__ = f.__doc__ + '\nReturns:\n\t' + str(self._returns)
        typedF.__returntype__ = self._returns

        if hasattr(f, '__params__'):
            typedF.__params__ = f.__params__

        return typedF

class Params(object):
    def __init__(self, *args, **kwargs):
        size1 = len(args)
        self._argTypes = list(args)
        self._kwargTypes = kwargs

        for i in range(len(self._argTypes)):
            if not isinstance(self._argTypes[i], Type):
                self._argTypes[i] = Type(self._argTypes[i])

        self._argTypes = tuple(self._argTypes)
        assert(size1 == len(self._argTypes))

    def __eq__(self, other):
        if len(self._argTypes) != len(other._argTypes):
            return False

        for i in range(len(self._argTypes)):
            if not self._argTypes[i] == other._argTypes[i]:
                return False

        return True

    def __repr__(self):
        return str(self._argTypes)

    def __str__(self):
        return self.__repr__()

    def _numRequiredArgs(self):
        total = 0
        for arg in self._argTypes:
            if not arg._isOptional:
                total += 1

        return total

    def _checkArgLength(self, args):
        if len(args) < self._numRequiredArgs():
            raise TypeError('Parameter list length mismatch: Expected at least: '
            + str(self._numRequiredArgs())
            + ', got: ' + str(len(args)))
        if len(args) > len(self._argTypes):
            raise TypeError('Parameter list length mismatch: Expected at most: '
            + str(len(self._argTypes))
            + ', got: ' + str(len(args)))

    def _checkArgTypes(self, f, args):
        for i in range(len(args)):
            typeCheckResult = self._argTypes[i].matches(args[i])
            if type(typeCheckResult) is TypeCheckingIterator:
                args[i] = typeCheckResult

            if not typeCheckResult:
                raise TypeError('Invalid parameter type for method: ' + f.__name__ + '. Expected: '
                + str(self._argTypes[i])
                + ', got: ' + str(Type(args[i])))

    def _checkKwargTypes(self, f, kwargs):
        for key, expectedType in self._kwargTypes.items():
            expectedType = Type(expectedType)
            actualValue = kwargs.get(key, None)
            typeCheckResult = expectedType.matches(actualValue)

            if type(typeCheckResult) is TypeCheckingIterator:
                kwargs[key] = typeCheckResult

            if not typeCheckResult:
                raise Py3TypingError('Invalid parameter type for method: {} kwarg: {}. Expected: '
                    .format(f.__name__, key)
                + str(expectedType)
                + ', got: ' + str(Type(actualValue)))

    def __call__(self, f):
        if not Typing.isEnabled():
            return f

        def g(*args, **kwargs):
            args = list(args)

            self._checkArgLength(args)
            self._checkArgTypes(f, args)
            self._checkKwargTypes(f, kwargs)            

            return f(*args, **kwargs)

        g.__name__ = getattr(f, '__name__', 'g')

        if f.__doc__ is None:
            f.__doc__ = ''

        g.__doc__ = f.__doc__ + '\nArgs:'
        
        for arg in self._argTypes:
            g.__doc__ += '\n\t' + str(arg)

        # TODO: Kwargs docs.

        g.__params__ = self

        if hasattr(f, '__returntype__'):
            g.__returntype__ = f.__returntype__

        return g
