# Typing

This project adds type annotations to Python3. It allows the enforcement of parameter and return types and also allows the creation of typed interfaces.

This is not an official Google product.

## Usage
```python
from py3typing.Typing import *
Typing.setEnabled(True) # Optionally disable in production.
# Other typed imports here

@Params(int, lol=optional(str))
@Returns(LIST.of(int))
def foo(bar, *, lol=None):
    return [1]
```
