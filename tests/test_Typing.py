'''
    Copyright 2016 Google Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
'''
from py3typing.Typing import *
from unittest import TestCase

Typing.setEnabled(True)

class BASIC_INTERFACE:
    @Returns(None)
    def foo(self):
        pass

    @Params(object, int)
    @Returns(int)
    def bar(self, b):
        pass

@Implements(BASIC_INTERFACE)
class VALID_IMPLEMENTATION:
    @Returns(None)
    def foo(self):
        pass

    @Returns(int)
    @Params(object, int)
    def bar(self, b):
        return 10

class Thrown:
    def __init__(self, instance, message):
        self._instance = instance
        self._message = message

    def withMessage(self, m):
        self._instance.assertEqual(self._message, m)

    def withMessageContaining(self, m):
        self._instance.assertTrue(m in self._message)

class test_Typing(TestCase):
    def assertThrows(self, f, err, *args):
        thrown = False
        try:
            f(*args)
        except err as e:
            thrown = True
            return Thrown(self, str(e))
        self.assertTrue(thrown)

    def assertNotThrows(self, f, err, *args):
        thrown = False
        try:
            f(*args)
        except err:
            thrown = True
        self.assertFalse(thrown)

    def test_Returns(self):
        @Returns(int)
        def f():
            return ''

        self.assertThrows(f, TypeError)

        @Returns(int)
        def g():
            return 0

        self.assertNotThrows(g, TypeError)

    def test_Returns_List(self):
        @Returns(LIST.of(str))
        def f():
            return ['1', '2', '3']

        self.assertNotThrows(f, TypeError)

        @Returns(LIST.of(str))
        def g():
            return ['1', 2, '3']

        self.assertThrows(g, TypeError) \
        .withMessage('Invalid return type for g. Expected: list of str, got: <class \'list\'>')

        @Returns(LIST.of(LIST.of(float)))
        def h():
            return [[6.28]]

        self.assertNotThrows(h, TypeError)

        @Returns(LIST.of(LIST.of(float)))
        def k():
            return [[6]]

        self.assertThrows(k, TypeError)

    def test_Params(self):
        @Params(int, str)
        def f(a, b):
            pass

        self.assertThrows(f, TypeError, '', '') \
        .withMessage('Invalid parameter type for method: f. Expected: int, got: str')

        self.assertNotThrows(f, TypeError, 1, '')

        class A:
            @Params(object)
            def f(self):
                pass

        a = A()
        self.assertNotThrows(a.f, TypeError)

        class B:
            @Params()
            def f(self):
                pass

        b = B()
        self.assertThrows(b.f, TypeError) \
        .withMessage('Parameter list length mismatch: Expected at most: 0, got: 1')

    def test_Params_kwargs(self):
        @Params(int, str, c=optional(bool))
        def f(a, b, *, c=True):
            pass

        def defaultArg():
            f(10, 'a')

        self.assertNotThrows(defaultArg, TypeError)

        def badArg():
            f(10, 'a', c=11)

        self.assertThrows(badArg, RuntimeError)

        def goodArg():
            f(10, 'a', c=False)

        self.assertNotThrows(goodArg, RuntimeError)

        class X:
            @Params(object, str, limit=optional(int), offset=optional(int))
            def bar(self, a, *, b=1, c=2):
                pass

        def missingArgs():
            X().bar('abcd')

        self.assertNotThrows(missingArgs, RuntimeError)

    def test_Params_Returns_staticmethod_classmethod(self):
        class TestClass:
            @staticmethod
            @Params(int)
            @Returns(int)
            def foo(x):
                return x * 2

            @classmethod
            @Params(object, int)
            @Returns(int)
            def lol(cls, x):
                return x * 2

            @Params(int)
            @Returns(int)
            @staticmethod
            def bar(x):
                return x * 2

        self.assertNotThrows(TestClass.foo, TypeError, 10)
        self.assertNotThrows(TestClass.lol, TypeError, 10)
        self.assertThrows(TestClass.bar, TypeError, 10)

    def test_Iterable(self):
        @Params(Iterable(map).of(int))
        @Returns(Iterable(list).of(int))
        def f(a):
            return list(a)

        @Params(Iterable(list).of(int))
        def g(a):
            return list(a)

        f(map(lambda x: x, [1, 2, 3, 4, 5]))
        g([1, 2, 3, 4, 5])

        def badParam():
            f(map(lambda x: x, [1, 2, 3, 'a', 5]))

        self.assertThrows(badParam, TypeError)

        def notIterable():
            f(5)
                
        self.assertThrows(notIterable, TypeError)

        def badIteratorType():
            f([1, 2, 3, 4, 5])

        self.assertThrows(badIteratorType, TypeError)

        @Params(Iterable(list).of(int))
        @Returns(Iterable(map).of(int))
        def foo(a):
            return a

        def badReturnType():
            list(foo([1, 2, 3]))

        self.assertThrows(badReturnType, TypeError)

        @Params(Iterable().of(int))
        @Returns(Iterable().of(int))
        def bar(a):
            return a

        bar([1, 2, 3, 4, 5])
        bar(map(lambda x: x, [1, 2, 3, 4, 5]))

    def test_Interface_unimplemented(self):
        @Implements(BASIC_INTERFACE)
        class B():
            pass

        self.assertThrows(B, NotImplementedError) \
        .withMessageContaining('does not implement bar')

    def test_Interface_wrongReturnType(self):
        @Implements(BASIC_INTERFACE)
        class D():
            @Returns(None)
            def foo(self):
                pass
            
            @Params(object, int)
            @Returns(str)
            def bar(self, b):
                pass

        self.assertThrows(D, TypeError) \
        .withMessage('bar\'s return type: str does not match int')

    def test_Interface_correctImplementation(self):
        @Implements(BASIC_INTERFACE)
        class C(object):
            def __init__(self, a):
                self._a = a

            @Returns(None)
            def foo(self):
                pass

            @Returns(int)
            @Params(object, int)
            def bar(self, b):
                return b

        def bar():
            c = C(10)
            self.assertEqual(c.bar(5), 5)
            self.assertEqual(c._a, 10)
        
        self.assertNotThrows(bar, NotImplementedError)

    def test_Impelements_ABS(self):
        @Implements(BASIC_INTERFACE)
        class C(BASIC_INTERFACE):
            def __init__(self, a):
                super().__init__()
                self._a = a

            @Returns(int)
            @Params(object, int)
            def bar(self, b):
                return b

        def bar():
            c = C(10)
            self.assertEqual(c.bar(5), 5)
            self.assertTrue(hasattr(c, '_a'))
        
        self.assertNotThrows(bar, NotImplementedError)

    def test_Interface_noReturnType(self):
        @Implements(BASIC_INTERFACE)
        class E():
            @Returns(None)
            def foo(self):
                pass

            @Params(int)
            def bar(self, b):
                pass

        self.assertThrows(E, TypeError) \
        .withMessage('bar does not have return type: int')

    def test_Interface_noParameters(self):
        @Implements(BASIC_INTERFACE)
        class D():
            @Returns(None)
            def foo(self):
                pass

            @Returns(int)
            def bar(self, b):
                return 10

        self.assertThrows(D, TypeError) \
        .withMessage('bar does not have parameter types: (object, int)')

    def test_Interface_badParameters(self):
        @Implements(BASIC_INTERFACE)
        class D():
            @Returns(None)
            def foo(self):
                pass

            @Returns(int)
            @Params(object, str)
            def bar(self, b):
                return 10

        self.assertThrows(D, TypeError) \
        .withMessage('bar\'s argument types: (object, str) does not match (object, int)')

    def test_Interface_interfaceReturnType(self):
        @Returns(BASIC_INTERFACE)
        def foo():
            return VALID_IMPLEMENTATION()

        self.assertNotThrows(foo, TypeError)

    def test_Interface_interfaceParamType(self):
        @Params(BASIC_INTERFACE)
        def foo(x):
            pass

        self.assertNotThrows(foo, TypeError, VALID_IMPLEMENTATION())
        self.assertThrows(foo, TypeError, 10)

    def test_disableTyping(self):
        Typing.setEnabled(False)

        @Implements(BASIC_INTERFACE)
        class A:
            pass

        self.assertNotThrows(A, NotImplementedError)

    def test_Interface_selfAsParam(self):
        class _A:
            pass

        class A(_A):
            @Params(object, _A)
            def f(self, a):
                pass

        @Implements(A)
        class A_impl:
            def __init__(self):
                pass

            @Params(object, A)
            def f(self, a):
                pass

        def foo():
            a = A_impl()
            a2 = A_impl()
            a.f(a2)

        def bar():
            A_impl().f(1)

        self.assertNotThrows(foo, TypeError)
        self.assertThrows(bar, TypeError) \
        .withMessage('Invalid parameter type for method: f. Expected: A, got: int')
