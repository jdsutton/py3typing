#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='py3typing',
      version='1.0',
      description='',
      author='John D. Sutton',
      author_email='',
      url='',
      packages=find_packages(),
      install_requires=[]
     )
